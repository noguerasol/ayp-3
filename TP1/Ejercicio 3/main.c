#include <stdio.h>

int main(){
    int tamanio;
    printf("Ejercicio 3: Mínimo \n");
    printf("A partir de un listado de números, determinar el mínimo. \n");
    printf("Cuanta cantidad de numeros va a tener su lista? \n");
    scanf("%d", &tamanio);
    int lista[tamanio-1];
    printf("Escriba un elemento de la lista y presione enter, hasta finalizarla. \n");
    int numElegido;
    int menor = 99999999;
    for(int i =0; i<tamanio; i++){
        printf("elemento %d : ", i+1);
        scanf("%d", &numElegido);
        lista[i] = numElegido;
        if (numElegido < menor){
            menor = numElegido;
        }
    }
    printf("El numero mas chico de la lista introducida fue %d", menor);
    return 0;
}