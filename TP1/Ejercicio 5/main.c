#include <stdio.h>

int main(){

    printf("Ejercicio 5: ASCII \n");
    printf("Imprimir todos los caracteres ASCII. \n");

    for (int i=0; i<=255; i++){
        printf("Numero %d ", i);
        printf(" = Caracter ASCII %c \n", i);
    }

    return 0;
}