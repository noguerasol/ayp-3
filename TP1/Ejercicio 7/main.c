#include <stdio.h>
#include <stdbool.h>

int main() {

    printf("Ejercicio 7: Menú \n");
    printf("Diseñar un menú navegable donde cada opción muestre una frase\n"
           "distinta. Debe retornar al menú a menos que se elija la opción “Salir”.\n");
    int opcion;
    bool activo = true;
    while(activo){
        printf("Elija una opción\n"
               "Opción 1.\n"
               "Opción 2.\n"
               "Opcion 3.\n"
               "Opción 4.\n"
               "Opción 5: Salir\n");
        scanf("%d", opcion);

        switch (opcion) {
            case 1:
                printf("Vive. El dinero se recupera, el tiempo no.");
                break;
            case 2:
                printf("La única diferencia entre un día malo y uno bueno es tu actitud.");
                break;
            case 3:
                printf("Ningún mar en calma hizo experto a un marinero.");
                break;
            case 4:
                printf("Intenta ser un Arco iris en el día nublado de alguien.");
                break;
            case 5:
                activo = false;
                break;
            default:
                printf("Opción inválida.\n Saliendo...");
                activo = false;
        }
    }
    
    return 0;
}