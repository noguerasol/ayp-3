#include <stdio.h>

int main() {

    printf("Ejercicio 6: Número par \n");
    printf("Determinar si el número ingresado es par o no. \n");
    int num;
    printf("Ingrese un número y nosotros le diremos si es par o no. \n");
    scanf("%d", num);

    if ((num % 2) == 0){
        printf("%d Es par.", num);
    } else {
        printf("%d No es par", num);
    }
    return 0;

}