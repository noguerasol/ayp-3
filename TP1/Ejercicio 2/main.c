#include <stdio.h>

int main(){
    int tamanio;
    printf("Ejercicio 2: Máximo \n");
    printf("A partir de un listado de números, determinar el máximo. \n");
    printf("Cuanta cantidad de numeros va a tener su lista? \n");
    scanf("%d", &tamanio);
    int lista[tamanio-1];
    printf("Escriba un elemento de la lista y presione enter, hasta finalizarla. \n");
    int numElegido;
    int mayor = -999999;
    for(int i =0; i<tamanio; i++){
        printf("elemento %d : ", i+1);
        scanf("%d", &numElegido);
        lista[i] = numElegido;
        if (numElegido > mayor){
            mayor = numElegido;
        }
    }
    printf("El numero mas grande de la lista introducida fue %d", mayor);
    return 0;
}