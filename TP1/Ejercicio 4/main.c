#include <stdio.h>

int main(){

    printf("Ejercicio 4: Promedio \n");
    printf("A partir del ingreso de tres enteros determinar el promedio \n");
    printf("Escriba un elemento de la lista de 3 enteros y presione enter, hasta finalizarla. \n");
    int numElegido;
    double prom = 0;
    for(int i =0; i<3; i++){
        printf("elemento %d : ", i+1);
        scanf("%d", &numElegido);
        prom = prom + numElegido;
    }
    prom = prom / 3;
    printf("El promedio entre los 3 enteros introducidos es %f", prom);
    return 0;
}