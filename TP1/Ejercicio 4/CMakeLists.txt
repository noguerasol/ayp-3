cmake_minimum_required(VERSION 3.17)
project(ayp_3 C)

set(CMAKE_C_STANDARD 11)

add_executable(ayp_3
        "main.c")
