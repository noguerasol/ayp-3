#include <stdio.h>
#include <stdbool.h>

//Defino la estructura

typedef struct{

    char  titulo[50];
    char  autor[50];
    int   paginas;

} Libro;

//Modularizo la iniciacion de los datos de la estructura

Libro setTitulo(Libro libro){
    libro.titulo = "Harry Potter y la camara secreta";
    return libro;
}

Libro setAutor(Libro libro){
    libro.autor = "JK Rowling";
    return libro;
}

Libro setPaginas(Libro libro){
    libro.paginas = 400;
    return libro;
}


int main{

    Libro libro2;
    libro2 = setAutor(libro2);
    libro2 = setTitulo(libro2);
    libro2 = setPaginas(libro2);

};
