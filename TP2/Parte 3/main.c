#include <stdio.h>
#include <stdbool.h>

//Estructura que representa una persona con hasta 2 hijos.
typedef struct {
    char *nombre;
    int edad;
} Hijo;

typedef struct {
    char *nombre;
    char *apellido;
    int edad;
    Hijo hijos[2];
} Persona;

int main(int argc, char *argv[]) {
    Hijo hija1 = {"Sol",  19};
    Hijo hijo2 = {"Pablo",  32};
    Hijo hijos[2];
    hijos[0] = hija1;
    hijos[1] = hijo2;
    Persona papa = {"Daniel", "Vazquez",55, hijos};
    printf("Nombre: %s; Edad: %d, hija 1: %s, hijo 2: %s\n", papa.nombre, papa.edad, papa.hijos[0].nombre, papa.hijos[1].nombre);
    return 0;
}
