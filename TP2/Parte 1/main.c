#include <stdio.h>
#include <stdbool.h>

int main() {

    //Defino Struct
    typedef struct{

    char * titulo;
    char * autor;
    int   paginas;

    } Libro;

    //Declaro y le doy valores
    Libro libro1;

    libro1.titulo = 'Harry Potter y la piedra filosofal';
    libro1.autor = 'JK Rowling';
    libro1.paginas = 300;

    //Imprimo los valores
    printf( "Libro 1 titulo : %s\n", libro1.titulo);
    printf( "Libro 1 autor : %s\n", libro1.autor);
    printf( "Libro 1 paginas : %s\n", libro1.paginas);

    return 0;


}
